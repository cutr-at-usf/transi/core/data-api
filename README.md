# Data API Service

### Program Type:

	Service

### Execution Frequency:

	N/A

### Program Description

This service delivers archived data from a MongoDB instance and provides dataset configurations to other jobs/services within the enviroment. The following file config/config.json is expected in order to read the dataset configuration used by Transi-Core. Refer to sample configuration on [getting started project](https://gitlab.com/cutr-at-usf/transi/core/getting-started) and variable explanation below.

It is recommended that services are managed through an orchestrator. Refer to [getting started project](https://gitlab.com/cutr-at-usf/transi/core/getting-started) for deployment information.

### Program Execution

```
docker run --rm --network=my_network \
-v /transi/config/data-api:/usr/src/app/config \
registry.gitlab.com/cutr-at-usf/transi/core/data-api
```

### Configuration Parameters

**DB_URL**: Base URL scheme used to log into a MongoDB database.

**POSTGRE_URL**: Base URL scheme used to log into a PostgreSQL database.

**data_name**: Name of dataset. External.

**timezone**: Timezone of dataset

**db**: Name used for database. Internal.

**DB_UPLOAD**: Boolean value to enable or disable PB upload to MongoDB

**PB_DOWNLOAD**: Boolean value to enable or disable PB upload to MongoDB

**SLEEP_TIME**: Polling interval for GTFS-RT feed

**SLEEP_ADAPTIVE**: Boolean value to enable or disable adaptive polling. SLEEP_TIME is baseline.

**URL_GTFS**: URL for GTFS feed

**URL_TRIP_UPDATES_ENABLED**: Boolean value to monitor trip_updates feed (true/false)

**URL_TRIP_UPDATES**: URL for trip_updates feed

**URL_VEHICLE_POSITIONS_ENABLED**: Boolean value to monitor vehicle_positions feed (true/false)

**URL_VEHICLE_POSITIONS**: URL for vehicle_positions feed

**URL_ALERTS_ENABLED**: Boolean value to monitor alerts feed (true/false)

**URL_ALERTS**: URL for alerts feed


Please refer to [getting started project](https://gitlab.com/cutr-at-usf/transi/core/getting-started) for more information about the system
