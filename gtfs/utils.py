from flask import Blueprint, jsonify, request
from config import mongo, config
from datetime import datetime
import flask_pymongo
import traceback
import pytz

calls = {
	'gtfs_timestamp':{
		'url':'/locate-timestamp', 
		'type':'feature',
		'method':'get',
		'cache-level':'trip_id',
		'required':['timestamp','source']
	}
}

api = Blueprint('gtfs_utils_api', __name__)

@api.route('/locate-timestamp', methods=['GET'])
def locate_gtfs_timestamp():
	try:
		timestamp = int(request.args.get('timestamp'))
		dataset = request.args.get('source')
		tz = pytz.timezone(config['entities'][dataset]['timezone'])
		dataset = config['entities'][dataset]['db']

		query = mongo.cx[dataset].calendar.aggregate(
			[
				{ "$sort": { "header.timestamp" : flask_pymongo.DESCENDING } },
				{ "$project": 
					{
						"_id": 0,
						"header": 1,
						"data.start_date": 1,
						"data.end_date": 1
					}
				}
			]
		)

		response_object = None
		response_status = "NO-RESULTS"
		for result in query:
			t_start = tz.localize(datetime.strptime(result['data'][0]['start_date'], "%Y%m%d")).timestamp()
			t_end = tz.localize(datetime.strptime(result['data'][0]['end_date'], "%Y%m%d")).timestamp()
			if timestamp >= t_start and timestamp <= t_end:
				return response(result['header']['timestamp'], "OK")
				response_object = result['header']['timestamp']
				response_status = "OK"

	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


def response(value, status_response):
    return jsonify({ "header": { "status": status_response }, "result" : value })

