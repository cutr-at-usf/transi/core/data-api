from .fetch import api as gtfs_fetch_api, calls as gtfs_fetch_calls
from .filter import api as gtfs_filter_api, calls as gtfs_filter_calls
from .aggregate import api as gtfs_aggregate_api, calls as gtfs_aggregate_calls
from .utils import api as gtfs_utils_api, calls as gtfs_utils_calls

def mount(app, url_prefix):
	app.register_blueprint(gtfs_fetch_api, url_prefix=url_prefix)
	app.register_blueprint(gtfs_filter_api, url_prefix=url_prefix+'/filter')
	app.register_blueprint(gtfs_aggregate_api, url_prefix=url_prefix+'/aggregate')
	app.register_blueprint(gtfs_utils_api, url_prefix=url_prefix+'/utils')
	return app

def getCalls(url_prefix):
	calls = {}

	for key, value in gtfs_fetch_calls.items():
		calls[key] = value
		calls[key]['url'] = url_prefix + calls[key]['url']

	for key, value in gtfs_filter_calls.items():
		calls[key] = value
		calls[key]['url'] = url_prefix + '/filter' + calls[key]['url']

	for key, value in gtfs_aggregate_calls.items():
		calls[key] = value
		calls[key]['url'] = url_prefix + '/aggregate' + calls[key]['url']

	for key, value in gtfs_utils_calls.items():
		calls[key] = value
		calls[key]['url'] = url_prefix + '/utils' + calls[key]['url']

	return calls

