from flask import Blueprint, jsonify, request
from config import mongo, config
import flask_pymongo
import traceback

calls = {
	'gtfs-filter-trip-info':{
		'url':'/trip-info', 
		'type':'data',
		'method':'get',
		'cache-level':'trip_id',
		'required':['gtfs_timestamp','trip_id','source']
	}
}

api = Blueprint('gtfs_filter_api', __name__)

@api.route('/trip-info', methods=['GET'])
def get_trip_info():
	try:
		dataset = config['entities'][request.args.get('source')]['db']
		timestamp = int(request.args.get('gtfs_timestamp'))

		trip_id = request.args.get('trip_id')
		response_object = mongo.cx[dataset].trips.aggregate(
			[
				{ "$match":
					{
						"header.timestamp": { "$lte": timestamp },
						"data.trips.trip_id" : trip_id
					}
				},
				{ "$sort": { "header.timestamp" : flask_pymongo.DESCENDING } },
				{ "$project": 
					{
						"_id": 0,
						"header": 1,
						"data.route_id": 1,
						"data.trip": {
							"$filter": {
								"input": "$data.trips",
								"as": "trip",
								"cond": { "$eq": [ "$$trip.trip_id", trip_id ] }
							}
						}
					}
				},
				{ "$limit" : 1 }
			]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "NO-RESULTS"
	return response(response_object, response_status)


# API RESPONSE FORMATTING #


def read_one(query_result):
	for result in query_result:
		return result
	return None


def send_response(output, status_response):
	if output is None:
		if status_response == "OK":
			status_response = "NO-RESULTS"
		output = { "header": { "status": status_response } }
	else:
		output['header']['status'] = status_response
	return jsonify(output)


def response(query_result, status_response):
	return send_response(read_one(query_result), status_response)

