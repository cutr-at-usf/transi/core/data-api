from flask import Blueprint, jsonify, request
from config import mongo, config
import flask_pymongo
import traceback

calls = {
	'gtfs-agency':{
		'url':'/agency', 
		'type':'data',
		'method':'get',
		'cache-level':'trip_id',
		'required':['gtfs_timestamp','source']
	},
	'gtfs-calendar':{
		'url':'/calendar', 
		'type':'data',
		'method':'get',
		'cache-level':'trip_id',
		'required':['gtfs_timestamp','source']
	},
	'gtfs-calendar-dates':{
		'url':'/calendar_dates', 
		'type':'data',
		'method':'get',
		'cache-level':'trip_id',
		'required':['gtfs_timestamp','source']
	},
	'gtfs-fare-attributes':{
		'url':'/fare_attributes', 
		'type':'data',
		'method':'get',
		'cache-level':'trip_id',
		'required':['gtfs_timestamp','source']
	},
	'gtfs-fare-rules':{
		'url':'/fare_rules', 
		'type':'data',
		'method':'get',
		'cache-level':'trip_id',
		'required':['gtfs_timestamp','source']
	},
	'gtfs-routes':{
		'url':'/routes', 
		'type':'data',
		'method':'get',
		'cache-level':'trip_id',
		'required':['gtfs_timestamp','source']
	},
	'gtfs-shapes':{
		'url':'/shapes', 
		'type':'data',
		'method':'get',
		'cache-level':'trip_id',
		'required':['gtfs_timestamp','shape_id','source']
	},
	'gtfs-stop-times':{
		'url':'/stop_times', 
		'type':'data',
		'method':'get',
		'cache-level':'trip_id',
		'required':['gtfs_timestamp','trip_id','source']
	},
	'gtfs-stops':{
		'url':'/stops', 
		'type':'data',
		'method':'get',
		'cache-level':'trip_id',
		'required':['gtfs_timestamp','stop_id','source']
	},
	'gtfs-trips':{
		'url':'/trips', 
		'type':'data',
		'method':'get',
		'cache-level':'trip_id',
		'required':['gtfs_timestamp','route_id','source']
	}
}

api = Blueprint('gtfs_fetch_api', __name__)

@api.route('/agency', methods=['GET'])
def get_agency():
	try:
		dataset = config['entities'][request.args.get('source')]['db']
		timestamp = int(request.args.get('gtfs_timestamp'))
		response_object = mongo.cx[dataset].agency.find_one(
			{"header.timestamp": timestamp}, {'_id': False},
			sort=[("header.timestamp", flask_pymongo.DESCENDING)]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/calendar', methods=['GET'])
def get_calendar():
	try:
		dataset = config['entities'][request.args.get('source')]['db']
		timestamp = int(request.args.get('gtfs_timestamp'))
		response_object = mongo.cx[dataset].calendar.find_one(
			{"header.timestamp": timestamp}, {'_id': False},
			sort=[("header.timestamp", flask_pymongo.DESCENDING)]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/calendar_dates', methods=['GET'])
def get_calendar_dates():
	try:
		dataset = config['entities'][request.args.get('source')]['db']
		timestamp = int(request.args.get('gtfs_timestamp'))
		response_object = mongo.cx[dataset].calendar_dates.find_one(
			{"header.timestamp": timestamp}, {'_id': False},
			sort=[("header.timestamp", flask_pymongo.DESCENDING)]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/fare_attributes', methods=['GET'])
def get_fare_attributes():
	try:
		dataset = config['entities'][request.args.get('source')]['db']
		timestamp = int(request.args.get('gtfs_timestamp'))
		response_object = mongo.cx[dataset].fare_attributes.find_one(
			{"header.timestamp": timestamp}, {'_id': False},
			sort=[("header.timestamp", flask_pymongo.DESCENDING)]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/fare_rules', methods=['GET'])
def get_fare_rules():
	try:
		dataset = config['entities'][request.args.get('source')]['db']
		timestamp = int(request.args.get('gtfs_timestamp'))
		response_object = mongo.cx[dataset].fare_rules.find_one(
			{"header.timestamp": timestamp}, {'_id': False},
			sort=[("header.timestamp", flask_pymongo.DESCENDING)]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/routes', methods=['GET'])
def get_routes():
	try:
		dataset = config['entities'][request.args.get('source')]['db']
		timestamp = int(request.args.get('gtfs_timestamp'))
		response_object = mongo.cx[dataset].routes.find_one(
			{"header.timestamp": timestamp}, {'_id': False},
			sort=[("header.timestamp", flask_pymongo.DESCENDING)]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/shapes', methods=['GET'])
def get_shapes():
	try:
		dataset = config['entities'][request.args.get('source')]['db']
		timestamp = int(request.args.get('gtfs_timestamp'))
		shape_id = request.args.get('shape_id')
		response_object = mongo.cx[dataset].shapes.find_one(
			{"header.timestamp": timestamp, "data.properties.shape_id": shape_id}, {'_id': False},
			sort=[("header.timestamp", flask_pymongo.DESCENDING)]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/stop_times', methods=['GET'])
def get_stop_times():
	try:
		dataset = config['entities'][request.args.get('source')]['db']
		timestamp = int(request.args.get('gtfs_timestamp'))
		trip_id = request.args.get('trip_id')
		response_object = mongo.cx[dataset].stop_times.find_one(
			{"header.timestamp": timestamp, "data.trip_id": trip_id}, {'_id': False},
			sort=[("header.timestamp", flask_pymongo.DESCENDING)]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/stops', methods=['GET'])
def get_stops():
	try:
		dataset = config['entities'][request.args.get('source')]['db']
		timestamp = int(request.args.get('gtfs_timestamp'))
		stop_id = request.args.get('stop_id')
		response_object = mongo.cx[dataset].stops.find_one(
			{"header.timestamp": timestamp, "data.properties.stop_id": stop_id}, {'_id': False},
			sort=[("header.timestamp", flask_pymongo.DESCENDING)]
		)
		response_status = "OK"
	except (ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "NO-RESULTS"
	return response(response_object, response_status)


@api.route('/trips', methods=['GET'])
def get_trips():
	try:
		timestamp = int(request.args.get('gtfs_timestamp'))
		route_id = request.args.get('route_id')
		dataset = config['entities'][request.args.get('source')]['db']
		response_object = mongo.cx[dataset].trips.find_one(
			{"header.timestamp": timestamp, "data.route_id": route_id}, {'_id': False},
			sort=[("header.timestamp", flask_pymongo.DESCENDING)]
		)
		response_status = "OK"
	except (ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "NO-RESULTS"
	return response(response_object, response_status)


# API RESPONSE FORMATTING #


def response(response_object, response_status):
	if response_status == "OK":
		response_object['header']['status'] = response_status
	else:
		response_object = { "header": { "status": response_status } }
	return jsonify(response_object)

