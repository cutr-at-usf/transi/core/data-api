from flask import Blueprint, jsonify, request
from config import mongo, config
import flask_pymongo
import traceback

calls = {
	'gtfs-aggregate-trip-stops':{
		'url':'/trip-stops', 
		'type':'data',
		'method':'post',
		'cache-level':'trip_id',
		'required':['gtfs_timestamp','source','gtfs-stop-times']
	}
}

api = Blueprint('gtfs_aggregate_api', __name__)

@api.route('/trip-stops', methods=['POST'])
def get_trip_stops():
	try:

		response_object = []
		for stop in request.json['gtfs-stop-times']['data']['stops']:

			stop_id = stop['stop_id']
			dataset = config['entities'][request.json['source']]['db']
			timestamp = int(request.json['gtfs_timestamp'])

			result = mongo.cx[dataset].stops.find_one(
				{"header.timestamp": {"$lte": timestamp}, "data.properties.stop_id": stop_id}, {'_id': False},
				sort=[("header.timestamp", flask_pymongo.DESCENDING)]
			)
			
			response_object.append(result)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


# API RESPONSE FORMATTING #


def response(value, status_response):
	return jsonify({ "header": { "status": status_response }, "result" : value })

