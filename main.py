from flask import Flask, jsonify
from config import mongo, config
from pathlib import Path
import importlib
import os

app = Flask(__name__)
calls = {}

app.config['MONGO_URI'] = config['config']['db_url']
mongo.init_app(app)

modules = os.listdir(os.getcwd())
modules = set(modules) - set(['__pycache__','config'])

for name in modules:
	try:
		if Path(name).is_dir():
			lib = importlib.import_module(name)
			app = lib.mount(app, "/"+name)
			calls.update(lib.getCalls("/"+name))
			print('Loaded '+name+' module')
	except ImportError:
		print('Error loading module '+name)

@app.route("/overview.json")
def overview():
    return jsonify({ 'api':calls })

if __name__ == '__main__':
	app.run(host= '0.0.0.0',port=80,debug=False)
