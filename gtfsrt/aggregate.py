from flask import Blueprint, jsonify, request
from config import mongo, config
from datetime import datetime as dt
import flask_pymongo
import traceback
import pytz

calls = {
	'gtfsrt-aggregate-trip-id':{
		'url':'/trip-id', 
		'type':'data',
		'method':'get',
		'cache-level':'trip_id',
		'required':['timestamp','status-config-timezone','source']
	},
	'gtfsrt-aggregate-trip-timestamps':{
		'url':'/trip-timestamps', 
		'type':'data',
		'method':'post',
		'cache-level':'trip_id',
		'required':['timestamp','trip_id','source','status-config-timezone']
	},
	'gtfsrt-aggregate-vehicle-positions':{
		'url':'/vehicle-positions', 
		'type':'data',
		'method':'get',
		'cache-level':'trip_id',
		'required':['trip_time_start','trip_time_complete','vehicle_id','source']
	},
	'gtfsrt-aggregate-vehicle-timestamps':{
		'url':'/vehicle-timestamps', 
		'type':'data',
		'method':'post',
		'cache-level':'trip_id',
		'required':['timestamp','vehicle_id','source','status-config-timezone']
	},
	'gtfsrt-aggregate-vehicle-count':{
		'url':'/vehicle-count', 
		'type':'data',
		'method':'get',
		'cache-level': None,
		'required':['from','to','source']
	},
	'gtfsrt-aggregate-vehicle-sample':{
		'url':'/vehicle-sample', 
		'type':'data',
		'method':'get',
		'cache-level': None,
		'required':['from','to','source']
	}
}

api = Blueprint('gtfsrt_aggregate_api', __name__)

@api.route('/trip-id', methods=['GET'])
def get_trip_id():
	try:
		timestamp = int(request.args.get('timestamp'))
		dataset = config['entities'][request.args.get('source')]['db']
		timezone = request.args.get('status-config-timezone')

		tz = pytz.timezone(timezone)
		datetime = dt.fromtimestamp(timestamp, tz)
		datetime = datetime.replace(hour=0, minute=0, second=0)

		timestamp_from = int(datetime.timestamp())
		timestamp_to = timestamp_from + 86400
		query = mongo.cx[dataset].trip_updates.distinct(
			"entity.tripUpdate.trip.tripId", 
			{ "header.timestamp": { "$gte": timestamp_from, "$lt":timestamp_to } }
		);

		response_object = query
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)

@api.route('/trip-timestamps', methods=['POST'])
def get_trip_updates():
	try:
		timestamp = request.json['timestamp']
		trip_id = request.json['trip_id']
		dataset = config['entities'][request.json['source']]['db']
		timezone = request.json['status-config-timezone']

		tz = pytz.timezone(timezone)
		datetime = dt.fromtimestamp(timestamp, tz)
		datetime = datetime.replace(hour=0, minute=0, second=0)

		timestamp_from = int(datetime.timestamp())
		timestamp_to = timestamp_from + 86400

		query = mongo.cx[dataset].trip_updates.find_one({ 
			"header.timestamp": { 
				"$gte": timestamp_from,
				"$lt":timestamp_to
			},
			"entity.tripUpdate.trip.tripId" : trip_id
		}, {"header": 1})

		timestamp_from = int(query['header']['timestamp']) - 21600
		timestamp_to = int(query['header']['timestamp']) + 21600

		query = mongo.cx[dataset].trip_updates.aggregate(
			[
				{
					"$match": { 
						"header.timestamp": { 
							"$gte": timestamp_from,
							"$lte":timestamp_to
						}, 
						"entity.tripUpdate.trip.tripId" : trip_id
					} 
				},
				{ "$sort": { "header.timestamp" : flask_pymongo.ASCENDING } },
				{ "$project": 
					{
						"_id": 0,
						"timestamp": "$header.timestamp"
					}
				}
			]
		)	

		response_object = []
		for result in query:
			response_object.append(result['timestamp'])
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/vehicle-positions', methods=['GET'])
def get_vehicle_positions():
	try:
		timestamp_from = int(request.args.get('trip_time_start'))
		timestamp_to = int(request.args.get('trip_time_complete'))
		vehicle_id = request.args.get('vehicle_id')
		dataset = config['entities'][request.args.get('source')]['db']

		query = mongo.cx[dataset].vehicle_positions.aggregate(
			[
				{
					"$match": { 
						"header.timestamp": { 
							"$gte": timestamp_from,
							"$lte":timestamp_to
						},
						"entity.vehicle.vehicle.id": vehicle_id
					} 
				},
				{ "$sort": { "header.timestamp" : flask_pymongo.ASCENDING } },
				{ "$project": 
					{
						"_id": 0,
						"header": 1,
						"entity": {
							"$filter": {
								"input": "$entity",
								"as": "entity",
								"cond": { "$eq": [ "$$entity.vehicle.vehicle.id", vehicle_id ] }
							}
						}
					}
				}
			]
		)	

		response_object = []
		for result in query:
			response_object.append(result)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/vehicle-timestamps', methods=['POST'])
def get_vehicle_timestamps():
	try:
		timestamp = request.json['timestamp']
		vehicle_id = request.json['vehicle_id']
		dataset = config['entities'][request.json['source']]['db']
		timezone = request.json['status-config-timezone']

		tz = pytz.timezone(timezone)
		datetime = dt.fromtimestamp(timestamp, tz)
		datetime = datetime.replace(hour=0, minute=0, second=0)

		timestamp_from = int(datetime.timestamp())
		timestamp_to = timestamp_from + 86400

		query = mongo.cx[dataset].vehicle_positions.find_one({ 
			"header.timestamp": { 
				"$gte": timestamp_from,
				"$lt":timestamp_to
			},
			"entity.vehicle.vehicle.id" : vehicle_id
		}, {"header": 1})

		timestamp_from = int(query['header']['timestamp']) - 21600
		timestamp_to = int(query['header']['timestamp']) + 21600
		query = mongo.cx[dataset].vehicle_positions.aggregate(
			[
				{
					"$match": { 
						"header.timestamp": { 
							"$gte": timestamp_from,
							"$lte":timestamp_to
						}, 
						"entity.vehicle.vehicle.id" : vehicle_id
					} 
				},
				{ "$sort": { "header.timestamp" : flask_pymongo.ASCENDING } },
				{ "$project": 
					{
						"_id": 0,
						"timestamp": "$header.timestamp"
					}
				}
			]
		)	

		response_object = []
		for result in query:
			response_object.append(result['timestamp'])
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/vehicle-count', methods=['GET'])
def get_vehicle_count():
	try:
		timestamp_from = int(request.args.get('from'))
		timestamp_to = int(request.args.get('to'))
		dataset = config['entities'][request.args.get('source')]['db']
		query = mongo.cx[dataset].vehicle_positions.aggregate(
			[
				{ 
					"$match": { 
						"header.timestamp": { 
							"$gt": timestamp_from,
							"$lte": timestamp_to
						}
					}
				},
				{ "$sort": { "header.timestamp" : flask_pymongo.DESCENDING } },
				{
					"$project": {
						"_id": 0,
						"timestamp": "$header.timestamp",
						"count": { "$size": {"$ifNull": [ "$entity", [] ] } }
					}
				}
			]
		)
		response_object = []
		for result in query:
			response_object.append(result)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/vehicle-sample', methods=['GET'])
def get_vehicle_sample():
	try:
		timestamp_from = int(request.args.get('from'))
		timestamp_to = int(request.args.get('to'))
		dataset = config['entities'][request.args.get('source')]['db']
		query = mongo.cx[dataset].vehicle_positions.aggregate(
			[
				{
					"$match": {
						"header.timestamp": {
							"$gt": timestamp_from,
							"$lte": timestamp_to
						}
					}
				},
				{ "$group": { "_id": "null", "count": { "$sum": 1 } } },
				{ "$project": { "_id": 0 } }
			]
		)
		output = []
		for result in query:
			output.append(result)
		if len(output) == 0:
			response_object = 0
		else:
			response_object = output[0]['count']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


# API RESPONSE FORMATTING #


def response(value, status_response):
	return jsonify({ "header": { "status": status_response }, "result" : value })

