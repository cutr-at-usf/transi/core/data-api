from .fetch import api as gtfsrt_fetch_api, calls as gtfsrt_fetch_calls
from .filter import api as gtfsrt_filter_api, calls as gtfsrt_filter_calls
from .aggregate import api as gtfsrt_aggregate_api, calls as gtfsrt_aggregate_calls

def mount(app, url_prefix):
	app.register_blueprint(gtfsrt_fetch_api, url_prefix=url_prefix)
	app.register_blueprint(gtfsrt_filter_api, url_prefix=url_prefix+'/filter')
	app.register_blueprint(gtfsrt_aggregate_api, url_prefix=url_prefix+'/aggregate')
	return app

def getCalls(url_prefix):
	calls = {}

	for key, value in gtfsrt_fetch_calls.items():
		calls[key] = value
		calls[key]['url'] = url_prefix + calls[key]['url']

	for key, value in gtfsrt_filter_calls.items():
		calls[key] = value
		calls[key]['url'] = url_prefix + '/filter' + calls[key]['url']

	for key, value in gtfsrt_aggregate_calls.items():
		calls[key] = value
		calls[key]['url'] = url_prefix + '/aggregate' + calls[key]['url']

	return calls

