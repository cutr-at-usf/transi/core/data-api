from flask import Blueprint, jsonify, request
from config import mongo, config
import flask_pymongo
import traceback

calls = {
    'gtfsrt-trip-updates':{
        'url':'/trip_updates', 
        'type':'data',
        'method':'get',
        'cache-level':'timestamp',
        'required':['timestamp','source']
    },
    'gtfsrt-vehicle-positions':{
        'url':'/vehicle_positions', 
        'type':'data',
        'method':'get',
        'cache-level':'timestamp',
        'required':['timestamp','source']
    },
    'gtfsrt-next-trip-updates':{
        'url':'/next_trip_updates', 
        'type':'data',
        'method':'get',
        'cache-level':'timestamp',
        'required':['timestamp','source']
    },
    'gtfsrt-next-vehicle-positions':{
        'url':'/next_vehicle_positions', 
        'type':'data',
        'method':'get',
        'cache-level':'timestamp',
        'required':['timestamp','source']
    }
}

api = Blueprint('gtfsrt_fetch_api', __name__)

@api.route('/trip_updates', methods=['GET'])
def get_trip_updates():
    try:
        timestamp = int(request.args.get('timestamp'))
        dataset = config['entities'][request.args.get('source')]['db']
        response_object = mongo.cx[dataset].trip_updates.find_one(
            {"header.timestamp": {"$lte": timestamp}}, {'_id': False},
            sort=[("header.timestamp", flask_pymongo.DESCENDING)]
        )
        response_status = "OK"
    except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
        print(traceback.format_exc())
        print(e)
        response_object = None
        response_status = "INVALID-ARGUMENTS"
    return response(response_object, response_status)


@api.route('/vehicle_positions', methods=['GET'])
def get_vehicle_positions():
    try:
        timestamp = int(request.args.get('timestamp'))
        dataset = config['entities'][request.args.get('source')]['db']
        response_object = mongo.cx[dataset].vehicle_positions.find_one(
            {"header.timestamp": {"$lte": timestamp}}, {'_id': False},
            sort=[("header.timestamp", flask_pymongo.DESCENDING)]
        )
        response_status = "OK"
    except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
        print(traceback.format_exc())
        print(e)
        response_object = None
        response_status = "INVALID-ARGUMENTS"
    return response(response_object, response_status)


@api.route('/next_trip_updates', methods=['GET'])
def get_next_trip_updates():
    try:
        timestamp = int(request.args.get('timestamp'))
        dataset = config['entities'][request.args.get('source')]['db']
        response_object = mongo.cx[dataset].trip_updates.find_one(
            {"header.timestamp": {"$gt": timestamp}}, {'_id': False},
            sort=[("header.timestamp", flask_pymongo.ASCENDING)]
        )
        response_status = "OK"
    except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
        print(traceback.format_exc())
        print(e)
        response_object = None
        response_status = "INVALID-ARGUMENTS"
    return response(response_object, response_status)


@api.route('/next_vehicle_positions', methods=['GET'])
def get_next_vehicle_positions():
    try:
        timestamp = int(request.args.get('timestamp'))
        dataset = config['entities'][request.args.get('source')]['db']
        response_object = mongo.cx[dataset].vehicle_positions.find_one(
            {"header.timestamp": {"$gt": timestamp}}, {'_id': False},
            sort=[("header.timestamp", flask_pymongo.ASCENDING)]
        )
        response_status = "OK"
    except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
        print(traceback.format_exc())
        print(e)
        response_object = None
        response_status = "INVALID-ARGUMENTS"
    return response(response_object, response_status)


# API RESPONSE FORMATTING #


def response(output, status_response):
    if output is None:
        if status_response == "OK":
            status_response = "NO-RESULTS"
        output = { "header": { "status": status_response } }
    else:
        output['header']['status'] = status_response
    return jsonify(output)

