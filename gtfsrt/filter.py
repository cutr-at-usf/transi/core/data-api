from flask import Blueprint, jsonify, request
from config import mongo, config
import flask_pymongo
import traceback

calls = {
	'gtfsrt-filter-vehicle-update':{
		'url':'/vehicle-update', 
		'type':'data',
		'method':'get',
		'cache-level':'timestamp',
		'required':['timestamp','vehicle_id','source']
	},
	'gtfsrt-filter-trip-update':{
		'url':'/trip-update', 
		'type':'data',
		'method':'get',
		'cache-level':'timestamp',
		'required':['timestamp','trip_id','source']
	},
	'gtfsrt-filter-vehicle-trip-update':{
		'url':'/vehicle-trip-update', 
		'type':'data',
		'method':'get',
		'cache-level':'timestamp',
		'required':['timestamp','vehicle_id','source']
	},
	'gtfsrt-filter-next-vehicle-update':{
		'url':'/next-vehicle-update', 
		'type':'data',
		'method':'get',
		'cache-level':'timestamp',
		'required':['timestamp','vehicle_id','source']
	},
	'gtfsrt-filter-next-trip-update':{
		'url':'/next-trip-update', 
		'type':'data',
		'method':'get',
		'cache-level':'timestamp',
		'required':['timestamp','trip_id','source']
	},
	'gtfsrt-filter-next-vehicle-trip-update':{
		'url':'/next-vehicle-trip-update', 
		'type':'data',
		'method':'get',
		'cache-level':'timestamp',
		'required':['timestamp','vehicle_id','source']
	}
}

api = Blueprint('gtfsrt_filter_api', __name__)

@api.route('/vehicle-update', methods=['GET'])
def get_vehicle_update():
	try:
		timestamp = int(request.args.get('timestamp'))
		vehicle_id = request.args.get('vehicle_id')
		dataset = config['entities'][request.args.get('source')]['db']
		response_object = mongo.cx[dataset].vehicle_positions.aggregate(
			[
				{ "$match": { "header.timestamp": { "$lte": timestamp } } },
				{ "$sort": { "header.timestamp" : flask_pymongo.DESCENDING } },
				{ "$project": 
					{
						"_id": 0,
						"header": 1,
						"entity": {
							"$filter": {
								"input": "$entity",
								"as": "entity",
								"cond": { "$eq": [ "$$entity.vehicle.vehicle.id", vehicle_id ] }
							}
						}
					}
				},
				{ "$limit" : 1 }
			]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/trip-update', methods=['GET'])
def get_trip_update():
	try:
		timestamp = int(request.args.get('timestamp'))
		trip_id = request.args.get('trip_id')
		dataset = config['entities'][request.args.get('source')]['db']
		response_object = mongo.cx[dataset].trip_updates.aggregate(
			[
				{ "$match": { "header.timestamp": { "$lte": timestamp } } },
				{ "$sort": { "header.timestamp" : flask_pymongo.DESCENDING } },
				{ "$project": 
					{
						"_id": 0,
						"header": 1,
						"entity": {
							"$filter": {
								"input": "$entity",
								"as": "entity",
								"cond": { "$eq": [ "$$entity.tripUpdate.trip.tripId", trip_id ] }
							}
						}
					}
				},
				{ "$limit" : 1 }
			]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/vehicle-trip-update', methods=['GET'])
def get_vehicle_trip_update():
	try:
		timestamp = int(request.args.get('timestamp'))
		vehicle_id = request.args.get('vehicle_id')
		dataset = config['entities'][request.args.get('source')]['db']
		response_object = mongo.cx[dataset].trip_updates.aggregate(
			[
				{ "$match": { "header.timestamp": { "$lte": timestamp } } },
				{ "$sort": { "header.timestamp" : flask_pymongo.DESCENDING } },
				{ "$project": 
					{
						"_id": 0,
						"header": 1,
						"entity": {
							"$filter": {
								"input": "$entity",
								"as": "entity",
								"cond": { "$eq": [ "$$entity.tripUpdate.vehicle.id", vehicle_id ] }
							}
						}
					}
				},
				{ "$limit" : 1 }
			]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/next-vehicle-update', methods=['GET'])
def get_next_vehicle_update():
	try:
		timestamp = int(request.args.get('timestamp'))
		vehicle_id = request.args.get('vehicle_id')
		dataset = config['entities'][request.args.get('source')]['db']
		response_object = mongo.cx[dataset].vehicle_positions.aggregate(
			[
				{ "$match": { "header.timestamp": { "$gt": timestamp } } },
				{ "$sort": { "header.timestamp" : flask_pymongo.ASCENDING } },
				{ "$project": 
					{
						"_id": 0,
						"header": 1,
						"entity": {
							"$filter": {
								"input": "$entity",
								"as": "entity",
								"cond": { "$eq": [ "$$entity.vehicle.vehicle.id", vehicle_id ] }
							}
						}
					}
				},
				{ "$limit" : 1 }
			]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/next-trip-update', methods=['GET'])
def get_next_trip_update():
	try:
		timestamp = int(request.args.get('timestamp'))
		trip_id = request.args.get('trip_id')
		dataset = config['entities'][request.args.get('source')]['db']
		response_object = mongo.cx[dataset].trip_updates.aggregate(
			[
				{ "$match": { "header.timestamp": { "$gt": timestamp } } },
				{ "$sort": { "header.timestamp" : flask_pymongo.ASCENDING } },
				{ "$project": 
					{
						"_id": 0,
						"header": 1,
						"entity": {
							"$filter": {
								"input": "$entity",
								"as": "entity",
								"cond": { "$eq": [ "$$entity.tripUpdate.trip.tripId", trip_id ] }
							}
						}
					}
				},
				{ "$limit" : 1 }
			]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/next-vehicle-trip-update', methods=['GET'])
def get_next_vehicle_trip_update():
	try:
		timestamp = int(request.args.get('timestamp'))
		vehicle_id = request.args.get('vehicle_id')
		dataset = config['entities'][request.args.get('source')]['db']
		response_object = mongo.cx[dataset].trip_updates.aggregate(
			[
				{ "$match": { "header.timestamp": { "$gt": timestamp } } },
				{ "$sort": { "header.timestamp" : flask_pymongo.ASCENDING } },
				{ "$project": 
					{
						"_id": 0,
						"header": 1,
						"entity": {
							"$filter": {
								"input": "$entity",
								"as": "entity",
								"cond": { "$eq": [ "$$entity.tripUpdate.vehicle.id", vehicle_id ] }
							}
						}
					}
				},
				{ "$limit" : 1 }
			]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


# API RESPONSE FORMATTING #


def read_one(query_result):
	for result in query_result:
		return result
	return None


def send_response(output, status_response):
	if output is None:
		if status_response == "OK":
			status_response = "NO-RESULTS"
		output = { "header": { "status": status_response } }
	else:
		output['header']['status'] = status_response
	return jsonify(output)


def response(query_result, status_response):
	return send_response(read_one(query_result), status_response)
