FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY main.py ./
COPY config.py ./
COPY status ./status
COPY gtfs ./gtfs
COPY gtfsrt ./gtfsrt

CMD [ "gunicorn", "--bind=0.0.0.0:80", "--workers=4", "main:app"]
