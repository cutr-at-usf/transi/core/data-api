from .config import api as status_config_api, calls as status_config_calls

def mount(app, url_prefix):
	app.register_blueprint(status_config_api, url_prefix=url_prefix+'/config')
	return app

def getCalls(url_prefix):
	calls = {}

	for key, value in status_config_calls.items():
		calls[key] = value
		calls[key]['url'] = url_prefix + '/config' + calls[key]['url']

	return calls

