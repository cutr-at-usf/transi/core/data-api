from flask import Blueprint, jsonify, request
from config import mongo, config
import flask_pymongo
import traceback

calls = {
	'status-config-timestamp-initial':{
		'url':'/timestamp-initial', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-timestamp-latest':{
		'url':'/timestamp-latest', 
		'type':'config',
		'method':'get',
		'cache-level':'timestamp',
		'required':['source']
	},
	'status-config-sources':{
		'url':'/sources', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':[]
	},
	'status-config-db-base-url':{
		'url':'/db-base-url', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':[]
	},
	'status-config-postgre-base-url':{
		'url':'/postgre-base-url', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':[]
	},
	'status-config-timezone':{
		'url':'/timezone', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-name':{
		'url':'/name', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-db-url':{
		'url':'/db-url', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-postgre-url':{
		'url':'/postgre-url', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-db-archive':{
		'url':'/db-archive', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-pb-archive':{
		'url':'/pb-archive', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-gtfsrt-request-interval':{
		'url':'/gtfsrt-request-interval', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-gtfsrt-request-adaptive':{
		'url':'/gtfsrt-request-adaptive', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-gtfsrt-vehicle-url':{
		'url':'/gtfsrt-vehicle-url', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-gtfsrt-vehicle-enabled':{
		'url':'/gtfsrt-vehicle-enabled', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-gtfsrt-trip-url':{
		'url':'/gtfsrt-trip-url', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-gtfsrt-trip-enabled':{
		'url':'/gtfsrt-trip-enabled', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-gtfsrt-alerts-url':{
		'url':'/gtfsrt-alerts-url', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-gtfsrt-alerts-enabled':{
		'url':'/gtfsrt-alerts-enabled', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	},
	'status-config-gtfs-url':{
		'url':'/gtfs-url', 
		'type':'config',
		'method':'get',
		'cache-level': None,
		'required':['source']
	}
}

api = Blueprint('status_config_api', __name__)

@api.route('/timestamp-initial', methods=['GET'])
def status_timestamp_initial():
	try:
		dataset = config['entities'][request.args.get('source')]['db']
		response_object = mongo.cx[dataset].vehicle_positions.find_one({}, 
			{"header.timestamp" : 1, "_id" : 0}, 
			sort=[("header.timestamp", flask_pymongo.ASCENDING)]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/timestamp-latest', methods=['GET'])
def status_timestamp_latest():
	try:
		dataset = config['entities'][request.args.get('source')]['db']
		response_object = mongo.cx[dataset].vehicle_positions.find_one({}, 
			{"header.timestamp" : 1, "_id" : 0}, 
			sort=[("header.timestamp", flask_pymongo.DESCENDING)]
		)
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/sources', methods=['GET'])
def status_sources():
	try:
		response_object = list(config['entities'].keys())
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/db-base-url', methods=['GET'])
def status_db_base_url():
	try:
		response_object = config['config']['db_url']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/postgre-base-url', methods=['GET'])
def status_postgre_base_url():
	try:
		response_object = config['config']['postgre_url']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/timezone', methods=['GET'])
def status_timezone():
	try:
		dataset = request.args.get('source')
		response_object = config['entities'][dataset]['timezone']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/name', methods=['GET'])
def status_name():
	try:
		dataset = request.args.get('source')
		response_object = config['entities'][dataset]['data_name']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/db-url', methods=['GET'])
def status_db_url():
	try:
		dataset = request.args.get('source')
		response_object = config['config']['db_url']+config['entities'][dataset]['db']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)

@api.route('/postgre-url', methods=['GET'])
def status_postgre_url():
	try:
		dataset = request.args.get('source')
		if config['config']['postgre_url'] is not None:
			response_object = config['config']['postgre_url']+config['entities'][dataset]['db']
		else:
			response_object = None
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)

@api.route('/db-archive', methods=['GET'])
def status_db_archive():
	try:
		dataset = request.args.get('source')
		response_object = config['entities'][dataset]['db_upload']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/pb-archive', methods=['GET'])
def status_pb_archive():
	try:
		dataset = request.args.get('source')
		response_object = config['entities'][dataset]['pb_download']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/gtfsrt-request-interval', methods=['GET'])
def status_gtfsrt_request_interval():
	try:
		dataset = request.args.get('source')
		response_object = config['entities'][dataset]['sleep_time']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/gtfsrt-request-adaptive', methods=['GET'])
def status_gtfsrt_request_adaptive():
	try:
		dataset = request.args.get('source')
		response_object = config['entities'][dataset]['sleep_adaptive']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/gtfsrt-vehicle-url', methods=['GET'])
def status_gtfsrt_vehicle_url():
	try:
		dataset = request.args.get('source')
		response_object = config['entities'][dataset]['url_vehicle_positions']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/gtfsrt-vehicle-enabled', methods=['GET'])
def status_gtfsrt_vehicle_enabled():
	try:
		dataset = request.args.get('source')
		response_object = config['entities'][dataset]['url_vehicle_positions_enabled']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/gtfsrt-trip-url', methods=['GET'])
def status_gtfsrt_trip_url():
	try:
		dataset = request.args.get('source')
		response_object = config['entities'][dataset]['url_trip_updates']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/gtfsrt-trip-enabled', methods=['GET'])
def status_gtfsrt_trip_enabled():
	try:
		dataset = request.args.get('source')
		response_object = config['entities'][dataset]['url_trip_updates_enabled']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/gtfsrt-alerts-url', methods=['GET'])
def status_gtfsrt_alerts_url():
	try:
		dataset = request.args.get('source')
		response_object = config['entities'][dataset]['url_alerts']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/gtfsrt-alerts-enabled', methods=['GET'])
def status_gtfsrt_alerts_enabled():
	try:
		dataset = request.args.get('source')
		response_object = config['entities'][dataset]['url_alerts_enabled']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


@api.route('/gtfs-url', methods=['GET'])
def status_gtfs_url():
	try:
		dataset = request.args.get('source')
		response_object = config['entities'][dataset]['url_gtfs']
		response_status = "OK"
	except (NameError, ValueError, TypeError, KeyError, IndexError) as e:
		print(traceback.format_exc())
		print(e)
		response_object = None
		response_status = "INVALID-ARGUMENTS"
	return response(response_object, response_status)


def response(value, status_response):
    return jsonify({ "header": { "status": status_response }, "result" : value })

